# sun

A small script to quickly print out sunrise and sunset times from https://api.sunrisesunset.io. 

## Requirements

It uses `bash`, but ought to work in other shells too.

- `curl`
- GNU `grep` or an equivalent
- optionally, `jq`

## Installation

Save the `sun` script to your computer. Make it executable with `chmod +x sun`.

Optionally, move it to a folder in your `$PATH` (e. g. `~/.local/bin`) and make sure your computer knows about it by doing `. .bash_profile`, or your system's equivalent.

## Configuration

Open the file in a text editor and replace the co-ordinates and place name at the beginning of the file with your own location's. The location name must be in quotes if it has spaces in it (e. g. "La Paz").

There are three versions of the script. You can enable or disable different ones by commenting/uncommenting lines (adding/removing the `#` at the beginning).

- The `jq` version is the one enabled by default.
- The one-line minimal version does not have formatting or emoji. 
- The no-`jq` version can be used if you can't or don't want to install `jq`. It uses `grep -P` instead. If your system uses a version of `grep` without Perl-style regices, you will have to find an equivalent command.

Of course, you can also customize the script however you like.

## Usage

Enter `./sun` at the command line. If the script is in your `$PATH`, simply enter `sun`.

## To do

Nothing. This is just a janky little script I made for myself because it takes slightly less effort than finding my phone and opening the weather app. I am genuinely surprised any other human beings on this planet pasted it into their terminal and ran it.

